import {StyleSheet, Text, View, ScrollView} from 'react-native';
import React from 'react';
import {Button, Chip, TextInput} from 'react-native-paper';
import axios from 'axios';
import {useEffect, useState} from 'react';
const getCatURL = 'https://62286b649fd6174ca82321f1.mockapi.io/case-study/categories/';

const Addproduct = () => {
  const [name, setName] = React.useState('');
  const [price, setprice] = React.useState('');
  const [description, setdescription] = React.useState('');
  const [imagelink, setimagelink] = React.useState('');
  const [data, setData] = React.useState([]);

  const fetchData = async () => {
    try {
      const {data: response} = await axios.get(getCatURL);
      setData(response);
      //console.log(response);
    } catch (error) {
      console.error(error.message);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  const categories = [];

  for(let i = 0; i<data.length; i++){
    categories.push(<Chip mode='outlined' style={{marginRight: 10}} onPress={() => console.log('Pressed')}>{data[i]['name']}</Chip>)
  }

  return (
    <ScrollView style={styles.container}>
      <TextInput
        style={styles.inputField}
        label="Name"
        mode="outlined"
        value={name}
        onChangeText={name => setName(name)}
      />
      <TextInput
        style={styles.inputField}
        label="Price"
        mode="outlined"
        value={price}
        onChangeText={price => setprice(price)}
      />

      <TextInput
        style={styles.inputField}
        label="Description"
        mode="outlined"
        value={description}
        onChangeText={description => setName(description)}
      />

      <TextInput
        style={styles.inputField}
        label="Image Link"
        mode="outlined"
        value={imagelink}
        onChangeText={imagelink => setimagelink(imagelink)}
      />
      <Text style={styles.title}>Selected Category:</Text>
      <ScrollView horizontal={true}
          showsHorizontalScrollIndicator={false} style={{flexDirection: 'row', marginBottom: 20}}>
        {categories}
      </ScrollView>
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <Button
          style={{width: '50%'}}
          mode="contained"
          onPress={() => console.log('Pressed')}>
          Add Product
        </Button>
      </View>
    </ScrollView>
  );
};

export default Addproduct;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  inputField: {
    marginTop: 15,
  },
  title: {
    marginVertical: 10,
    marginTop: 20,
    fontSize: 15,
  },
});
