import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  useWindowDimensions,
  TouchableOpacity,
} from 'react-native';
import {Chip} from 'react-native-paper';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import {useEffect, useState} from 'react';
const getProductsURL = 'https://62286b649fd6174ca82321f1.mockapi.io/case-study/products/';
const getCatURL = 'https://62286b649fd6174ca82321f1.mockapi.io/case-study/categories/';

const ListingData = props => {
  const navigation = useNavigation();
  //console.log(props.datashare);
  let testContent = [];
  for (let i = 0; i < props.datashare.length; i++) {
    let avatar = props.datashare[i]['avatar'];
    testContent.push(
      <View style={styles.listingWrap}>
        <Image
          style={{resizeMode: 'center', height: 100, marginBottom: 50}}
          source={{
            uri: avatar,
          }}
        />
        <View>
          <Text>{props.datashare[i]['name']}</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>${props.datashare[i]['price']}</Text>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Productdetails', {
                  itemId: props.datashare[i]['id'],
                  name: props.datashare[i]['name'],
                  price: props.datashare[i]['price'],
                  description: props.datashare[i]['description'],
                  avatar: avatar,
                })
              }>
              <Text>View</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>,
    );
  }
  return <View style={styles.rowDesign}>{testContent}</View>;
};

const Home = ({navigation}) => {
  const [data, setData] = React.useState([]);
  const [catdata, setcatdata] = React.useState([]);

  const fetchData = async () => {
    try {
      const {data: response} = await axios.get(getProductsURL);
      setData(response);
      //console.log(response);
    } catch (error) {
      console.error(error.message);
    }
  };

  const fetchcatData = async () => {
    try {
      const {data: response} = await axios.get('https://62286b649fd6174ca82321f1.mockapi.io/case-study/categories/');
      setcatdata(response);
      console.log(response);
    } catch (error) {
      console.error(error.message);
    }
  };

  useEffect(() => {
    fetchcatData();
    fetchData();
  }, []);

  const categories = [];

  for(let j = 0; j<catdata.length; j++){
    categories.push(<Chip mode='outlined' style={styles.chips} onPress={() => console.log('Pressed')}><Text style={styles.catText}>{catdata[j]['name']}</Text></Chip>)
  }

  return (
    <ScrollView style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 20,
        }}>
        <Text>UPayments Store</Text>
        <Text>Search</Text>
      </View>
      <ScrollView  horizontal={true}
          showsHorizontalScrollIndicator={false} style={{flexDirection: 'row', marginBottom: 20}}>
        {categories}
      </ScrollView>
      <View>
        <ListingData datashare={data} />
      </View>
      <TouchableOpacity
        style={styles.addButton}
        onPress={() => navigation.navigate('Addproduct')}>
        <Text style={{color: '#fff', fontSize: 20}}>Add</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  rowDesign: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  listingWrap: {
    width: '47%',
    padding: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: 10,
    borderRadius: 10,
  },
  addButton: {
    position: 'absolute',
    top: '90%',
    right: 10,
    backgroundColor: '#000',
    color: '#fff',
    padding: 30,
    borderRadius: 50,
  },
  chips: {
    backgroundColor: 'black',
    color: '#fff',
    marginRight: 10,
  },
  inputField: {
    marginTop: 15,
  },
  title: {
    marginVertical: 10,
    marginTop: 20,
    fontSize: 15,
  },
  catText: {
    color: '#fff',
  },
});
