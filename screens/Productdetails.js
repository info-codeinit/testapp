import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  useWindowDimensions,
} from 'react-native';
import React from 'react';

const Productdetails = ({route, navigation}) => {
  const { itemId, name, price, description, avatar } = route.params;
  const windowWidth = useWindowDimensions().width;
  let windowHeight = useWindowDimensions().height/2;

  return (
    <ScrollView style={styles.container}>
      <Image
        style={{resizeMode: 'contain', height: windowWidth - 100}}
        source={{
          uri: avatar,
        }}
      />
      <View style={[styles.desWrap, {minHeight: windowHeight}]}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.price}>${price}</Text>
        </View>
        <Text style={styles.description}>{description}</Text>
      </View>
    </ScrollView>
  );
};

export default Productdetails;

const styles = StyleSheet.create({
  container: {},
  title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  price: {
    color: '#fff',
    fontSize: 17,
    fontWeight: 'bold',
    marginTop: 4,
  },
  description: {
    color: '#fff',
    marginTop: 20,
  },
  desWrap: {
    marginTop: 20,
    padding: 20,
    backgroundColor: '#000',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
});
